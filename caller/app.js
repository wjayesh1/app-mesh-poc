// import express
var express = require('express');

var app = express();
// define port to start server on
const port = 5000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const axios = require('axios');

// URL to be hit
svcURL = 'http://servicea.local:5000/';

axios
  .post(svcURL, {
    data: 'dummy message'
  })
  .then(res => {
    console.log(`statusCode: ${res.statusCode}`)
    console.log(res)
  })
  .catch(error => {
    console.log("error faced")
    console.error(error)
  })

// start server on port
app.listen(port, () => {
  console.log(`app listening at http://localhost:${port}`);
})

// error handler
app.use(function(err, _req, res, _next) {
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
